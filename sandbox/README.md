This is more of for my use, but I'm realizing now that my usage of SELinux sandboxes for Firefox and Thunderbird are going to impact some Fedora packages eventually due to the changes I've been making to policycoreutils-sandbox so I decided to put some of this more public as I use it.

There are three bugs with the sandbox utility that I've identified.  One is that Xephyr grabs mouse input and traps the mouse cursor (easily preventable with an option, and solved in these scripts).  One is that Xephyr after extended use fails to recognize mouse inputs (somewhat attempted to mitigate at this point, I'm not sure where my status is on this).  The third is that the sandbox scripts can't effectively quit GUI applications about 50% of the time and when GUI windows are closed, the processes below them stay running (like Firefox) yet not attached to X.

I also required some extra policies to make sure Firefox and Thunderbird would work effectively inside the sandboxes.  Not amazing solutions at the moment, but temporarily it works.  There are better ways to do some of the things I'm doing but I haven't taken the time to fix them.

/bin - Fedora's policycoreutils-sandbox utilities changed to fix certain problems

/sandpol - My SELinux policies to temporarily get things to work until I clean them up

/scripts - My scripts for copying clipboards over from one sandbox to another and clearing all sandbox clipboards (requires zenity and xsel)
