#!/bin/bash

function sandbox_list(){
	# find all sandbox start commands (sandboxed SELinux windows)
	sandbox_pids=$(
		ps -Ao pid,cmd | \
			grep "sandbox/[s]tart" | \
			sed -r "s/^[ ]*([0-9]+) .*$/\1/g"
	)

	# loop pids
	tmpfile_loop=$(mktemp /tmp/sandbox.XXXXXXXXXX)
	echo "$sandbox_pids" | while read pid; do
		child_pid=$(ps --ppid $pid -o pid=)
		real_child=$(
			ps --ppid $child_pid -o pid=,cmd= | \
				grep -v openbox
		)
		real_child_pid=$(echo "$real_child" | sed -r "s/^[ ]*([0-9]+).*$/\1/g")
		real_child_name=$(echo "$real_child" | sed -r "s/^.*\/([^\/]+)$/\1/g")

		if [[ ! -z "$real_child_name" ]]; then
			echo $real_child_pid $real_child_name >> $tmpfile_loop
		fi
	done

	# grab children from inner loop (dumb hack)
	children=$(cat $tmpfile_loop)
	rm -f $tmpfile_loop

	# loop to get DISPLAYs
	echo "$children" | while read line; do
		pid=$(echo "$line" | cut -d ' ' -f1)
		name=$(echo "$line" | cut -d ' ' -f2-)
		display=$(sed -r "s/^.*DISPLAY=:([0-9]+).*$/\1/g" /proc/$pid/environ)
		echo $display $display \"$name\" >> $tmpfile_loop
	done

	sort $tmpfile_loop
	rm -f $tmpfile_loop
}
