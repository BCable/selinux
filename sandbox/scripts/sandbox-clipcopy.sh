#!/bin/bash

from="0"
to="0"

source "$(dirname $0)/sandbox-list.sh"
args=$(sandbox_list | tr "\n" " ")

from=$(bash -c "zenity --list --radiolist --text='Copy From:' \
	--column='' --column='D' --column='Application' \
	0 0 Global \
	$args \
	2> /dev/null
")

if [[ -z "$from" ]]; then
	exit 1
fi

to=$(bash -c "zenity --list --radiolist --text='Copy To:' \
	--column='' --column='D' --column='Application' \
	0 0 Global \
	$args \
	2> /dev/null
")

if [[ -z "$to" ]]; then
	exit 1
fi

# do final copy between the two displays
xsel --display :$from -o -b | xsel --display :$to -i -b
