#!/bin/bash

source "$(dirname $0)/sandbox-list.sh"

displays=$(sandbox_list | sed -r "s/\"[^\"]*\"//g" | sed -r "s/[ ]+/ /g")

i=0
for display in $displays; do
	if [[ "$i" = "0" ]]; then
		echo -n "" | xsel --display :$display -i -b
		i=1
	else
		i=0
	fi
done

# clear global clipboard
echo -n "" | xsel --display :0 -i -b
